# 分享红包营销中的用户博弈模型——以外卖分享红包为例

## 本科毕业论文，聊以纪念

Latex写作，Texlive2017下应可正常编译。

xelatex -> bibtex -> xelatex*2

FINAL_VERSION.pdf 为最终打印版论文。

数据提取、预处理等操作已略去，此处直接提供处理后的必须的.csv文件。

-----

### 主要文件简介

| 文件(夹)名 | 简介 |
|-------|------|
| FINAL_VERSION.pdf | Release版论文 |
| draft1_0307.tex | latex代码 |
| data/* | 数据集 |
| data/survey.csv | 实验数据集 |
| data/wxdata.csv | 微信数据集 |
| material/* | 论文引用图片，包括代码生成统计图和外部图片 |
| code/* | 一些代码，主要生成统计图像 |
| code/desc.py | 实验对象行为随最大红包金额的变动模式图 |
| code/hierarchy_cluster.py | 层级聚类 |
| code/wx_desc.py | 微信数据集描述性统计图 |
| refs.bib | 引用文献库 |
|...|封面配置、TeX同步等等|