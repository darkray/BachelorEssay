import numpy as np
import matplotlib as mpl
mpl.use('Qt5Agg')
# mpl.rcParams['text.usetex'] = True
# mpl.rcParams['text.latex.unicode'] = True
# mpl.rcParams['text.latex.preamble'] = [
#     '\\usepackage{CJK}',
#     r'\AtBeginDocument{\begin{CJK}{UTF8}{STSong}}',
#     r'\AtEndDocument{\end{CJK}}'
#     ]
mpl.rcParams['font.sans-serif'] = ['STSong']
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42
# mpl.rcParams['axes.unicode_minux'] = False
import matplotlib.pyplot as plt
import pandas as pd


dfs = pd.read_csv('../data/survey.csv')
dfwx = pd.read_csv('../data/wxdata.csv')
dfs['wait_time'] /= 1000
