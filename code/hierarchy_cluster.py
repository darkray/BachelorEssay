from init import *
import scipy.cluster.hierarchy as ch


t = dfs[np.abs(dfs.wait_time - dfs.wait_time.mean()) < 3 * dfs.wait_time.std()]
tt = t['wait_time']
z = ch.linkage(tt.values.reshape(-1, 1), 'average', optimal_ordering=True)
r = ch.dendrogram(z, p=4, truncate_mode='level', color_threshold=2.5, show_leaf_counts=True)
plt.axhline(2.5, color='k', ls='dashed')
locs, labels = plt.yticks()
plt.yticks(np.append(locs, 2.5))
plt.title(r'等待时间$n$聚类分析树')
plt.xlabel('叶子结点包含样本数目')
plt.ylabel('类间距离')
plt.show()

labels = 5 - ch.fcluster(z, 2.5, 'distance')

# colors = np.array(['red', 'orange', 'blue', 'black'])
# plt.scatter(tt, t['max_num'], c=colors[labels-1], alpha=0.8)
# plt.title('等待时间$t$分类散点图')
# plt.xlabel('等待时间')
# plt.ylabel('最大红包金额')
# plt.show()

cated_wt = pd.DataFrame(tt, labels)
gs = tt.groupby(labels)
groups_info = pd.DataFrame({'delta_med': gs.median() - gs.median().shift(1).fillna(0), 'group': [1, 2, 3, 4], 'mean': gs.mean(), 'range': gs.max() - gs.min(), 'median': gs.median(), 'n': gs.size(), 'max': gs.max(), 'min': gs.min(), 'delta_mean': gs.mean() - gs.mean().shift(1).fillna(0)},
columns=['group', 'n', 'min', 'mean', 'median', 'max', 'range', 'delta_mean', 'delta_med'])
print(groups_info.to_latex(index=False))
