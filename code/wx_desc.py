from init import *
from datetime import date


dfwx['weekday'] = dfwx.apply(lambda x: date(x['year'], x['month'], x['day']).weekday(), axis=1)

ax = plt.subplot2grid((2,2), (0,0))
ax.hist(dfwx['maxPos'], bins=range(3, 17), align='left')
ax.set_xlabel('最大红包位置')
ax.set_xticks(range(1, 13))
ax.set_ylabel('频数')
ax.set_title('外卖红包按最大红包位置分布频数直方图')

ax = plt.subplot2grid((2,2), (0,1))
ax.hist(dfwx['weekday'], bins=range(8), align='left')
ax.set_xlabel('星期')
ax.set_xticks(range(7))
ax.set_xticklabels(['一', '二', '三', '四', '五', '六', '日'])
ax.set_title('外卖红包按星期分布频数直方图')

ax = plt.subplot2grid((2,2), (1,0), colspan=2)
ax.hist2d(dfwx['minute'], dfwx['hour'], normed=True, bins=[range(61), range(25)])
ax.set_title('外卖红包消息在一天中的分布')
ax.set_xlabel('分钟')
ax.set_ylabel('小时')

plt.show()
