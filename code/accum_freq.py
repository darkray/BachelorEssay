from init import *

x=[0, 1,2,3]

y1 = [191,238,252,255]

y2=[183,243,252,253]

plt.plot(x, y1, 'r', label='真实')

plt.plot(x, y2, 'b',label='拟合')

plt.legend()

plt.title('模型拟合累积频数对比')

plt.xlabel('认知深度')

plt.ylabel('频数')

plt.show()