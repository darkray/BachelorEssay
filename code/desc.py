from init import *


t = dfs[np.abs(dfs.wait_time - dfs.wait_time.mean()) < 3 * dfs.wait_time.std()]

ax = plt.gca()
color = np.array(['black', 'blue', 'orange', 'red'])
_, inv = np.unique(t['max_value'], return_inverse=True)
color = color[inv]
ax.scatter(t['wait_time'], t['max_value'], c=color, alpha=0.5)

t.sort_values('max_value').groupby('open_id').plot(x='wait_time', y='max_value', ax=ax, legend=False, color='black', lw=0.5, alpha=0.5)

plt.xlabel('等待时间$t$')
plt.ylabel('最大红包金额')
plt.title('实验对象行为模式图')
plt.show()
